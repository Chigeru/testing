package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class PN extends Ordination {
	private final double antalEnheder;
	private int gangeGivet = 0;

	public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen. Returnerer true,
	 * hvis givesDen er inden for ordinationens gyldighedsperiode, og datoen huskes.
	 * Returnerer false ellers og datoen givesDen ignoreres.
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isBefore(getSlutDen().plusDays(1)) && givesDen.isAfter(getStartDen().minusDays(1))) {
			gangeGivet++;
			return true;
		}
		return false;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt.
	 */
	public int getAntalGangeGivet() {
		return gangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public double samletDosis() {
		return antalEnheder * gangeGivet;
	}

	@Override
	public double doegnDosis() {
		return gangeGivet * antalEnheder / ((int) ChronoUnit.DAYS.between(getStartDen(), getSlutDen()));
	}

	@Override
	public String getType() {
		return "PN";
	}
}
