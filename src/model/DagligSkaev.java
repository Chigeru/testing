package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private LocalTime tid;
	private double antal;
	private ArrayList<Dosis> Dosis = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);

	}

	/** Pre: antal >= 0. */
	public void opretDosis(LocalTime tid, double antal) {
		assert antal >= 0;
		Dosis dosis = new Dosis(tid, antal);
		Dosis.add(dosis);
	}

	public LocalTime getTid() {
		return tid;
	}

	public double getAntal() {
		return antal;
	}

	public ArrayList<Dosis> getDosis() {
		return new ArrayList<>(Dosis);
	}

	public void removeDosis(Dosis dosis) {
		Dosis.remove(dosis);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * ((int) ChronoUnit.DAYS.between(getStartDen(), getSlutDen()));
	}

	@Override
	public double doegnDosis() {
		double samlet = 0;
		for (int i = 0; i < Dosis.size(); i++) {
			samlet += Dosis.get(i).getAntal();
		}
		return samlet;
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}
}
