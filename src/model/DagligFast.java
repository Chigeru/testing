package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination {
	public DagligFast(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	private final Dosis[] dosises = new Dosis[4];

	// -------------------------------------------------------------------------

	public Dosis[] getDoser() {
		return dosises;
	}

	public Dosis createDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		if (antal < 0) {
			dosis = null;
		}

		if (tid == LocalTime.of(6, 00)) {
			dosises[0] = dosis;
		} else if (tid == LocalTime.of(12, 00)) {
			dosises[1] = dosis;
		} else if (tid == LocalTime.of(18, 00)) {
			dosises[2] = dosis;
		} else if (tid == LocalTime.of(00, 00)) {
			dosises[3] = dosis;
		}

		return dosis;
	}

	public void removeDosis(Dosis dosis) {
		for (int i = 0; i < dosises.length; i++) {
			if (dosises[i].equals(dosis)) {
				dosises[i] = null;
			}
		}
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * ((int) ChronoUnit.DAYS.between(getStartDen(), getSlutDen()));
	}

	@Override
	public double doegnDosis() {
		double samlet = 0;
		for (int i = 0; i < dosises.length; i++) {
			if (dosises[i] != null)
				samlet += dosises[i].getAntal();
		}
		return samlet;
	}

	@Override
	public String getType() {
		return "Daglig fast";
	}
}
