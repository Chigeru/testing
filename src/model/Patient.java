package model;

import java.util.ArrayList;

public class Patient {
    private final String cprnr;
    private final String navn;
    private final double vaegt;
    
    // association: --> 0..* Ordination
    private ArrayList<Ordination> ordinationer = new ArrayList<>();

    /** Pre: cpnnr ikke tomt, navn ikke tomt, vaegt >= 0. */
    public Patient(String cprnr, String navn, double vaegt) {
        assert !cprnr.isEmpty();
        assert !navn.isEmpty();
        assert vaegt >= 0;
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
    }
    
    public ArrayList<Ordination> getOrdinationer() {
        return new ArrayList<>(ordinationer);
    }
    
    public void addOrdination(Ordination ordination) {
        ordinationer.add(ordination);
    }
    
    public void removeOrdination(Ordination ordination) {
        ordinationer.remove(ordination);
    }

    public String getCprnr() {
        return cprnr;
    }

    public String getNavn() {
        return navn;
    }

    public double getVaegt() {
        return vaegt;
    }

    @Override
    public String toString() {
        return navn + "  " + cprnr;
    }
}
