package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

public class PNTest {

    private PN p;

    @Before
    public void setUp() throws Exception {
        p = new PN(LocalDate.of(2018, 02, 20), LocalDate.of(2018, 03, 20), 2.00);
    }

    @Test
    public void testSamletDosis() {
        p.givDosis(LocalDate.of(2018, 02, 25));
        p.givDosis(LocalDate.of(2018, 02, 25));
        p.givDosis(LocalDate.of(2018, 02, 25));

        assertEquals(2 * 3, p.getAntalGangeGivet() * p.getAntalEnheder(), 0.001);
    }

    @Test
    public void testDoegnDosis() {
        p.givDosis(LocalDate.of(2018, 02, 25));
        p.givDosis(LocalDate.of(2018, 02, 26));
        p.givDosis(LocalDate.of(2018, 02, 26));
        assertEquals(
                p.getAntalGangeGivet() * p.getAntalEnheder()
                        / ChronoUnit.DAYS.between(LocalDate.of(2018, 02, 20), LocalDate.of(2018, 03, 20)),
                p.doegnDosis(), 0.001);
    }

    @Test
    public void testGivDosis() {

        assertTrue(p.givDosis(LocalDate.of(2018, 02, 25)));
    }

    @Test
    public void testGetAntalGangeGivet() {
        p.givDosis(LocalDate.of(2018, 02, 25));
        p.givDosis(LocalDate.of(2018, 02, 25));
        p.givDosis(LocalDate.of(2018, 02, 25));
        p.givDosis(LocalDate.of(2018, 02, 25));
        p.givDosis(LocalDate.of(2018, 02, 25));

        assertEquals(5, p.getAntalGangeGivet(), 0.001);
    }

    @Test
    public void testGetAntalEnheder() {
        assertEquals(2, p.getAntalEnheder(), 0.001);
    }

}
