package model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {

    private DagligSkaev skaev;
    private DagligSkaev skaev2;

    @Before
    public void setup() throws Exception {
        skaev = new DagligSkaev(LocalDate.of(2018, 02, 20), LocalDate.of(2018, 02, 22));
        skaev2 = new DagligSkaev(LocalDate.of(2018, 03, 01), LocalDate.of(2018, 03, 07));

    }

    @Test
    public void DagligSkaev() {
        assertEquals(LocalDate.of(2018, 02, 20), skaev.getStartDen());
        assertEquals(LocalDate.of(2018, 02, 22), skaev.getSlutDen());

        assertEquals(LocalDate.of(2018, 03, 01), skaev2.getStartDen());
        assertEquals(LocalDate.of(2018, 03, 07), skaev2.getSlutDen());
    }

	@Test(expected = IllegalArgumentException.class)
	public void testDagligSkaevEx() {
		// assert TC1 - Slut dato er før startdato -> IllegalArgumentException
		new DagligSkaev(LocalDate.of(2018, 01, 28), LocalDate.of(2018, 01, 20));
	}
    
    @Test
    public void testSamletDosis() {
        skaev.opretDosis(LocalTime.of(06, 00), 2.00);
        skaev.opretDosis(LocalTime.of(12, 00), 2.00);
        skaev.opretDosis(LocalTime.of(16, 00), 2.00);
        skaev.opretDosis(LocalTime.of(20, 00), 2.00); // 4 dosiser

        assertEquals(4 * 2 * ChronoUnit.DAYS.between(LocalDate.of(2018, 02, 20), LocalDate.of(2018, 02, 22)),
                skaev.samletDosis(),
                0.001);

        skaev2.opretDosis(LocalTime.of(0, 00), 2.00);
        skaev2.opretDosis(LocalTime.of(3, 00), 2.00);
        skaev2.opretDosis(LocalTime.of(6, 00), 2.00);
        skaev2.opretDosis(LocalTime.of(9, 00), 2.00);
        skaev2.opretDosis(LocalTime.of(12, 00), 2.00);
        skaev2.opretDosis(LocalTime.of(15, 00), 2.00);
        skaev2.opretDosis(LocalTime.of(18, 00), 2.00);
        skaev2.opretDosis(LocalTime.of(21, 00), 2.00); // 8 dosiser

        assertEquals(8 * 2 * ChronoUnit.DAYS.between(LocalDate.of(2018, 03, 01), LocalDate.of(2018, 03, 07)),
                skaev2.samletDosis(),
                0.001);

    }

    @Test
    public void testDoegnDosis() {
        skaev.opretDosis(LocalTime.of(06, 00), 2.00);
        skaev.opretDosis(LocalTime.of(20, 00), 2.00); // 2 dosiser sammenlagt
        assertEquals(2 * 2.00, skaev.doegnDosis(), 0.001);

        skaev.opretDosis(LocalTime.of(06, 00), 2.00);
        skaev.opretDosis(LocalTime.of(12, 00), 2.00);
        skaev.opretDosis(LocalTime.of(16, 00), 2.00);
        skaev.opretDosis(LocalTime.of(20, 00), 2.00); // 6 dosiser sammenlagt
        assertEquals(6 * 2.00, skaev.doegnDosis(), 0.001);
    }

}
