package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {
	private DagligFast d1;

	@Before
	public void setUp() throws Exception {
		d1 = new DagligFast(LocalDate.of(2018, 01, 20), LocalDate.of(2018, 01, 22));
	}

	@Test
	public void testDagligFast() {
		// assert TC1
		assertEquals(d1.getStartDen(), LocalDate.of(2018, 01, 20));
		assertEquals(d1.getSlutDen(), LocalDate.of(2018, 01, 22));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDagligFastEx() {
		// assert TC1 - Slut dato er før startdato -> IllegalArgumentException
		new DagligFast(LocalDate.of(2018, 01, 28), LocalDate.of(2018, 01, 20));
	}

	@Test
	public void testSamletDosis() {
		// act
		d1.createDosis(LocalTime.of(06, 00), 1);
		d1.createDosis(LocalTime.of(12, 00), 2);
		d1.createDosis(LocalTime.of(18, 00), 0);
		d1.createDosis(LocalTime.of(00, 00), -1);

		// assert
		double doegnDosis = d1.doegnDosis();
		assertEquals(doegnDosis, 3, 0.001);
		// assert TC1
		assertEquals(d1.samletDosis(), 3 * 2, 0.001);

	}

	@Test
	public void testDoegnDosis() {
		// act
		d1.createDosis(LocalTime.of(06, 00), 1);
		d1.createDosis(LocalTime.of(12, 00), 0);
		d1.createDosis(LocalTime.of(18, 00), 0);
		d1.createDosis(LocalTime.of(00, 00), 0);

		// assert TC1
		assertEquals(d1.doegnDosis(), 1, 0.001);

		// act TC2
		d1.createDosis(LocalTime.of(12, 00), 1);
		// assert TC2
		assertEquals(d1.doegnDosis(), 2, 0.001);

		// act TC3
		d1.createDosis(LocalTime.of(18, 00), 1);
		// assert TC3
		assertEquals(d1.doegnDosis(), 3, 0.001);

		// act TC4
		d1.createDosis(LocalTime.of(00, 00), 1);
		// assert TC4
		assertEquals(d1.doegnDosis(), 4, 0.001);

		// act TC5
		d1.createDosis(LocalTime.of(12, 00), 0);
		d1.createDosis(LocalTime.of(18, 00), 2);
		d1.createDosis(LocalTime.of(00, 00), 5);
		// assert TC5
		assertEquals(d1.doegnDosis(), 8, 0.001);

		// act TC6
		d1.createDosis(LocalTime.of(06, 00), -1);
		d1.createDosis(LocalTime.of(12, 00), -1);
		d1.createDosis(LocalTime.of(18, 00), -1);
		d1.createDosis(LocalTime.of(00, 00), -1);
		// assert TC6
		assertEquals(d1.doegnDosis(), 0, 0.001);

	}

	@Test
	public void testCreateDosis() {

		// act TC1
		Dosis dosis1 = d1.createDosis(LocalTime.of(6, 0), 0);

		// assert TC1 - Dosis oprettet med rigtig info
		assertEquals(dosis1.getAntal(), 0, 0.001);
		assertEquals(dosis1.getTid(), LocalTime.of(6, 0));

		// assert TC1 - Dosis indsat rigtigt i array
		assertEquals(d1.getDoser()[0], dosis1);
		assertNull(d1.getDoser()[1]);
		assertNull(d1.getDoser()[2]);
		assertNull(d1.getDoser()[3]);

		// act TC2
		Dosis dosis2 = d1.createDosis(LocalTime.of(12, 0), 1);

		// assert TC2 - Dosis oprettet med rigtig info
		assertEquals(dosis2.getAntal(), 1, 0.001);
		assertEquals(dosis2.getTid(), LocalTime.of(12, 0));

		// assert TC2 - Dosis indsat rigtigt i array
		assertEquals(d1.getDoser()[0], dosis1);
		assertEquals(d1.getDoser()[1], dosis2);
		assertNull(d1.getDoser()[2]);
		assertNull(d1.getDoser()[3]);

		// act TC3
		Dosis dosis3 = d1.createDosis(LocalTime.of(18, 0), -1);

		// assert TC3 - Dosis oprettet med rigtig info
		assertNull(dosis3);

		// assert TC3 - Dosis indsat rigtigt i array
		assertEquals(d1.getDoser()[0], dosis1);
		assertEquals(d1.getDoser()[1], dosis2);
		assertEquals(d1.getDoser()[2], dosis3);
		assertNull(d1.getDoser()[3]);

		// act TC4
		Dosis dosis4 = d1.createDosis(LocalTime.of(00, 0), 1);

		// assert TC4 - Dosis oprettet med rigtig info
		assertEquals(dosis4.getAntal(), 1, 0.001);
		assertEquals(dosis4.getTid(), LocalTime.of(00, 0));

		// assert TC4 - Dosis indsat rigtigt i array
		assertEquals(d1.getDoser()[0], dosis1);
		assertEquals(d1.getDoser()[1], dosis2);
		assertEquals(d1.getDoser()[2], dosis3);
		assertEquals(d1.getDoser()[3], dosis4);
	}

}
