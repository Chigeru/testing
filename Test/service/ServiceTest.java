package service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import model.DagligFast;
import model.DagligSkaev;
import model.Laegemiddel;
import model.PN;
import model.Patient;
import storage.Storage;

public class ServiceTest {
    private Patient p1;
    private Laegemiddel l1;
    private Service s1;
    //private PN pn1;

    @Before
    public void setUp() throws Exception {
        l1 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        p1 = new Patient("070985-1153", "Finn Madsen", 83.2);
        s1 = new Service(new Storage());
    }

    @Test
    public void testOpretPNOrdination() {
        // act TC1
        PN pn1 = s1.opretPNOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 2), p1, l1, 1);
        // assert TC1
        assertTrue(p1.getOrdinationer().contains(pn1));        
        assertEquals(LocalDate.of(2018, 1, 1), pn1.getStartDen());
        assertEquals(LocalDate.of(2018, 1, 2), pn1.getSlutDen());
        assertEquals(l1, pn1.getLaegemiddel());
        assertEquals(pn1.getAntalEnheder(), 1, 0.001);
        
        
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretOrdinationPNEx() {
        // assert TC2 - Slut dato er før startdato -> IllegalArgumentException
        s1.opretPNOrdination(LocalDate.of(2018, 1, 2), LocalDate.of(2018, 1, 1), p1, l1, 1);
        
    }
    
    @Test
    public void testOpretDagligFastOrdination() {
        // act TC1
        DagligFast f1 = s1.opretDagligFastOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 2), p1, l1, 1, 2, 3, 4);
        // assert TC1
        assertTrue(p1.getOrdinationer().contains(f1));        
        assertEquals(LocalDate.of(2018, 1, 1), f1.getStartDen());
        assertEquals(LocalDate.of(2018, 1, 2), f1.getSlutDen());
        assertEquals(f1.getDoser()[0].getAntal(), 1, 0.001);
        assertEquals(f1.getDoser()[1].getAntal(), 2, 0.001);
        assertEquals(f1.getDoser()[2].getAntal(), 3, 0.001);
        assertEquals(f1.getDoser()[3].getAntal(), 4, 0.001);
        
        // act TC2
        DagligFast f2 = s1.opretDagligFastOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 2), p1, l1, -1, -1, -1, -1);
        // assert TC2
        assertNull(f2.getDoser()[0]);
        assertNull(f2.getDoser()[1]);
        assertNull(f2.getDoser()[2]);
        assertNull(f2.getDoser()[3]);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testOpretOrdinationFastEx() {
        // assert TC3 - Slut dato er før startdato -> IllegalArgumentException
    	s1.opretDagligFastOrdination(LocalDate.of(2018, 1, 2), LocalDate.of(2018, 1, 1), p1, l1, 1, 1, 1, 1);
        
    }
    

    @Test
    public void testOpretDagligSkaevOrdination() {
       //act TC1
    	LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
        DagligSkaev ds1 = s1.opretDagligSkaevOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 2), p1, l1, kl, an);
        
        //Assert TC1
        assertTrue(p1.getOrdinationer().contains(ds1));        
        assertEquals(LocalDate.of(2018, 1, 1), ds1.getStartDen());
        assertEquals(LocalDate.of(2018, 1, 2), ds1.getSlutDen());
        assertEquals(ds1.getDosis().size(), 4);
        assertEquals(ds1.getDosis().get(0).getTid(), LocalTime.of(12, 0));
        assertEquals(ds1.getDosis().get(1).getTid(), LocalTime.of(12, 40));
        assertEquals(ds1.getDosis().get(2).getTid(), LocalTime.of(16, 0));
        assertEquals(ds1.getDosis().get(3).getTid(), LocalTime.of(18, 45));
        assertEquals(ds1.getDosis().get(0).getAntal(), 0.5, 0.001);
        assertEquals(ds1.getDosis().get(1).getAntal(), 1, 0.001);
        assertEquals(ds1.getDosis().get(2).getAntal(), 2.5, 0.001);
        assertEquals(ds1.getDosis().get(3).getAntal(), 3, 0.001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testOpretOrdinationSkaevEx() {
        // assert TC2 - Slut dato er før startdato -> IllegalArgumentException
    	LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
    	s1.opretDagligSkaevOrdination(LocalDate.of(2018, 1, 2), LocalDate.of(2018, 1, 1), p1, l1, kl, an);
        
    }

    @Test
    public void testOrdinationPNAnvendt() {
        // act TC1
    	PN pn1 = s1.opretPNOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 5), p1, l1, 1);
    	 
    	 s1.ordinationPNAnvendt(pn1, LocalDate.of(2018, 1, 2));
    	 // assert TC1
    	 assertEquals(pn1.getAntalGangeGivet(), 1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testOrdinationPNAnvendtEx() {
        // act TC2
    	PN pn1 = s1.opretPNOrdination(LocalDate.of(2018, 1, 2), LocalDate.of(2018, 1, 5), p1, l1, 1);
    	// assert TC2 - dato før ordinations startsdato  -> IllegalArgumentException
    	s1.ordinationPNAnvendt(pn1, LocalDate.of(2018, 1, 1));       
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrdinationPNAnvendtEx2() {
        // act TC3
    	PN pn1 = s1.opretPNOrdination(LocalDate.of(2018, 1, 2), LocalDate.of(2018, 1, 5), p1, l1, 1);
    	// assert TC3 - dato efter ordinationens slutdato -> IllegalArgumentException
    	s1.ordinationPNAnvendt(pn1, LocalDate.of(2018, 1, 6));       
    }
    
    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel() {
    	
    	// act TC1
        Patient pa = s1.opretPatient("121256-0512", "Jane Jensen", 63.4);
        Laegemiddel la = s1.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        //assert TC1
    	assertEquals(s1.antalOrdinationerPrVægtPrLægemiddel(0, 100, la),0);
        
    	// act TC2
        s1.opretDagligFastOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 2), pa, la, 1, 2, 3, 4);   	
    	//assert TC2
    	assertEquals(s1.antalOrdinationerPrVægtPrLægemiddel(0, 100, la),1);
    }

}



